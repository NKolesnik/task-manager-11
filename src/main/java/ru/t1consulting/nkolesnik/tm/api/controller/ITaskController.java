package ru.t1consulting.nkolesnik.tm.api.controller;

public interface ITaskController {

    void createTask();

    void showTaskList();

    void clearTaskList();

    void showTaskByIndex();

    void showTaskById();

    void updateTaskByIndex();

    void updateTaskById();

    void removeTaskByIndex();

    void removeTaskById();

}
