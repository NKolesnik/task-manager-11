package ru.t1consulting.nkolesnik.tm.api.controller;

public interface IProjectController {

    void createProject();

    void showProjectList();

    void clearProjectList();

    void showProjectByIndex();

    void showProjectById();

    void updateProjectByIndex();

    void updateProjectById();

    void removeProjectByIndex();

    void removeProjectById();

}
